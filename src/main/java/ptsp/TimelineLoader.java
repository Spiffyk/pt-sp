package ptsp;

import lombok.NonNull;
import ptsp.network.Packet;
import ptsp.timeline.Frame;

import java.io.*;
import java.util.*;

/**
 * A utility class for reading timelines (collections of {@link Frame}s) from files.
 */
public class TimelineLoader {

	private static final String DATA_SEPARATOR = "\t-\t";

	/**
	 * Reads a timeline from the specified {@link File}.
	 *
	 * @param file the file to read from
	 * @return the timeline
	 */
	public static Collection<Frame> parseTimeline(@NonNull final File file) {
		try (final BufferedReader br = new BufferedReader(new FileReader(file))) {
			return parseTimeline(br);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found!", e);
		} catch (IOException e) {
			throw new RuntimeException("Could not read file!", e);
		}
	}

	/**
	 * Reads a timeline from the specified {@link BufferedReader}.
	 *
	 * @param br the buffered reader to read from
	 * @return the timeline
	 * @throws IOException should an I/O error arise while reading
	 */
	public static Collection<Frame> parseTimeline(@NonNull final BufferedReader br) throws IOException {
		String line;
		int lineNumber = 1;

		final Map<Integer, Frame> frameMap = new TreeMap<>();

		while ((line = br.readLine()) != null) {
			final String[] split = line.split(DATA_SEPARATOR);
			if (split.length != 4) {
				throw new InputFormatException(String.format(TimelineLoader.class.getSimpleName() + ": Line %d is in a wrong format!", lineNumber));
			}
			final int time = Integer.parseInt(split[0]);
			final int routerFrom = Integer.parseInt(split[1]);
			final int routerTo = Integer.parseInt(split[2]);
			final int size = Integer.parseInt(split[3]);

			final Frame frame;
			if(frameMap.containsKey(time))
			{
				frame = frameMap.get(time);
			}
			else
			{
				frame = new Frame(time);
				frameMap.put(time, frame);
			}

			frame.getPackets().add(new Packet(UUID.randomUUID().toString(), routerTo, routerFrom, size));

			lineNumber++;
		}

		return frameMap.values();
	}
}
