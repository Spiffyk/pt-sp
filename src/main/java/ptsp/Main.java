package ptsp;

import ptsp.network.Network;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Program entry point
 */
public class Main {

	/**
	 * Program entry point.
	 *
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		if (args.length < 2) {
			System.err.println("Two arguments required! (Graph and timeline files)");
			System.exit(1);
		}

		final Network network = NetworkLoader.parseNetwork(new File(args[0]));
		network.getTimeline().addAll(TimelineLoader.parseTimeline(new File(args[1])));

		int remainingTicks = 0;

		while (!network.isSimulationDone()) {
			if (remainingTicks == 0) {
				System.out.print(
						"----------\n" +
						" - How many ticks should be processed?\n" +
						" - (Set to 0 to run until there is nothing to do)\n" +
						" - (Enter an empty line to run one tick)\n" +
						" >> ");
				try {
					final String line = br.readLine();
					if (line.trim().isEmpty()) {
						remainingTicks = 1;
					} else {
						remainingTicks = Integer.parseInt(line);
					}
				} catch (IOException e) {
					throw new RuntimeException(e);
				} catch (NumberFormatException e) {
					System.err.println("Not a valid number. Quitting.");
					System.exit(2);
				}

				if (remainingTicks == 0) {
					remainingTicks = -1;
				} else if (remainingTicks < 0) {
					System.err.println("Negative number. Quitting.");
					System.exit(3);
				}
			}

			network.tick();
			if (remainingTicks > 0) {
				remainingTicks--;
			}
		}

		System.out.println("----------\nSimulation finished.");
		System.out.printf("Sent: %d\nDelivered: %d\nLost: %d\n",
				network.getSentPackets(), network.getFinishedPackets().size(), network.getLostPackets().size());
	}

}
