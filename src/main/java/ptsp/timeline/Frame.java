package ptsp.timeline;

import lombok.Data;
import ptsp.network.Packet;

import java.util.ArrayList;
import java.util.List;

/**
 * A frame containing packets to be sent at the given time.
 */
@Data
public class Frame
{
    /**
     * The time to send the packets.
     */
    private final int time;

    /**
     * The packets to be sent.
     */
    private final List<Packet> packets = new ArrayList<>();
}
