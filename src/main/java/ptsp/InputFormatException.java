package ptsp;

/**
 * A runtime exception thrown if an input file for {@link NetworkLoader} or {@link TimelineLoader} is incorrectly
 * formatted.
 */
public class InputFormatException extends RuntimeException {

	/**
	 * @see InputFormatException#InputFormatException()
	 */
	public InputFormatException() {
		super();
	}

	/**
	 * @see InputFormatException#InputFormatException(String)
	 */
	public InputFormatException(String message) {
		super(message);
	}

	/**
	 * @see InputFormatException#InputFormatException(String, Throwable)
	 */
	public InputFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @see InputFormatException#InputFormatException(Throwable)
	 */
	public InputFormatException(Throwable cause) {
		super(cause);
	}

	/**
	 * @see InputFormatException#InputFormatException(String, Throwable, boolean, boolean)
	 */
	public InputFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
