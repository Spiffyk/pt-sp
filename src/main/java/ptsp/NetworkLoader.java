package ptsp;

import lombok.NonNull;
import ptsp.network.Line;
import ptsp.network.Network;
import ptsp.network.Router;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A utility class for reading {@link Network}s from files.
 */
public class NetworkLoader {

	private static final String DATA_SEPARATOR = "\t-\t";

	/**
	 * Reads a network from the specified {@link File}.
	 *
	 * @param file the file to read from
	 * @return the network
	 */
	public static Network parseNetwork(@NonNull final File file) {
		try (final BufferedReader br = new BufferedReader(new FileReader(file))) {
			return parseNetwork(br);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("File not found!", e);
		} catch (IOException e) {
			throw new RuntimeException("Could not read file!", e);
		}
	}

	/**
	 * Reads a network from the specified {@link BufferedReader}.
	 *
	 * @param br the buffered reader to read from
	 * @return the network
	 * @throws IOException should an I/O error arise while reading
	 */
	public static Network parseNetwork(@NonNull final BufferedReader br) throws IOException {
		String line;
		int lineNumber = 1;

		int maxRouterIndex = 0;
		final List<Line.Wrapper> wrapperList = new ArrayList<>();

		while ((line = br.readLine()) != null) {
			final String[] split = line.split(DATA_SEPARATOR);
			if (split.length != 4) {
				throw new InputFormatException(String.format(NetworkLoader.class.getSimpleName() + ": Line %d is in a wrong format!", lineNumber));
			}
			final int routerFrom = Integer.parseInt(split[0]);
			final int routerTo = Integer.parseInt(split[1]);
			final int capacity = Integer.parseInt(split[2]);
			final double robustness = Double.parseDouble(split[3]);

			maxRouterIndex = Math.max(maxRouterIndex, Math.max(routerFrom, routerTo));
			wrapperList.add(new Line.Wrapper(routerFrom, routerTo, new Line(capacity, robustness)));

			lineNumber++;
		}

		return wrappersToNetwork(wrapperList, maxRouterIndex + 1);
	}

	/**
	 * Creates a {@link Network} from an {@link Iterable} of {@link ptsp.network.Line.Wrapper}s.
	 *
	 * @param wrappers an iterable of line wrappers
	 * @param noOfRouters number of routers in the network
	 * @return a new network
	 */
	private static Network wrappersToNetwork(
			@NonNull final Iterable<Line.Wrapper> wrappers, final int noOfRouters) {
		final Router[] routers = new Router[noOfRouters];
		final Map<Integer, Line>[] lineMaps = new Map[noOfRouters];
		for (int i = 0; i < lineMaps.length; i++) {
			lineMaps[i] = new HashMap<>();
		}

		for (final Line.Wrapper wrapper : wrappers) {
			lineMaps[wrapper.getRouterFrom()].put(wrapper.getRouterTo(), wrapper.getLine());
			lineMaps[wrapper.getRouterTo()].put(wrapper.getRouterFrom(), wrapper.getLine());

			if (routers[wrapper.getRouterTo()] == null) {
				routers[wrapper.getRouterTo()] = new Router();
			}
			if (routers[wrapper.getRouterFrom()] == null) {
				routers[wrapper.getRouterFrom()] = new Router();
			}
		}

		return new Network(routers, lineMaps);
	}
}
