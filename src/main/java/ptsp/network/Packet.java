package ptsp.network;

import lombok.Data;

/**
 * A data structure of a packet.
 */
@Data
public class Packet {

	/**
	 * An identifier of the packet.
	 */
	private final String uuid;

	/**
	 * The ID of the destination router.
	 */
	private final int receiverId;

	/**
	 * The ID of the starting router.
	 */
	private final int senderId;

	/**
	 * The size of the packet.
	 */
	private final int size;

	/**
	 * How much of the packet has been received.
	 */
	private int received = 0;

	/**
	 * How many times the packet has been stuck.
	 */
	private int noOfFails = 0;

	/**
	 * Adds the specified number to {@link Packet#received}.
	 *
	 * @param toAdd the number to add
	 */
	public void addReceived(final int toAdd) {
		received += toAdd;
	}

	/**
	 * Increments {@link Packet#noOfFails}.
	 */
	public void incrementFails() {
		noOfFails++;
	}

}
