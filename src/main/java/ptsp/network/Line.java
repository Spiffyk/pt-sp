package ptsp.network;

import lombok.Data;

/**
 * A line between {@link Router}s.
 */
@Data
public class Line {

	/**
	 * The capacity of the line.
	 */
	private final int capacity;

	/**
	 * The robustness of the line. How likely it is that a {@link Fragment} does not get lost if the line is more than
	 * 50% used up.
	 */
	private final double robustness;

	/**
	 * The ussage of the line.
	 */
	private int usage = 0;

	/**
	 * Resets the line's usage to 0.
	 */
	public void reset() {
		this.usage = 0;
	}

	/**
	 * Gets the remaining capacity of the line.
	 *
	 * @return the remaining capacity of the line
	 */
	public int getAvailable() {
		return capacity - usage;
	}

	/**
	 * Adds the specified number to the line's usage.
	 *
	 * @param toAdd the number
	 */
	public void addUsage(final int toAdd) {
		this.usage += toAdd;
	}

	/**
	 * A wrapper around a {@link Line} for loading purposes.
	 */
	@Data
	public static class Wrapper {

		/**
		 * Router 1.
		 */
		private final int routerFrom;

		/**
		 * Router 2.
		 */
		private final int routerTo;

		/**
		 * The line.
		 */
		private final Line line;

	}

}
