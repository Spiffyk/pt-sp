package ptsp.network;

import lombok.Data;

/**
 * A router class.
 */
@Data
public class Router {

	/**
	 * The capacity of each of the routers.
	 */
	private static final int CAPACITY = 100_000;

	/**
	 * The usage of the router.
	 */
	private int usage = 0;

	/**
	 * Tries to add the specified number to the usage.
	 *
	 * @param toAdd the number to add
	 * @return {@link true} if addition succeeds, {@link false} if it does not.
	 */
	public boolean tryAdd(final int toAdd) {
		if (usage + toAdd <= CAPACITY) {
			usage += toAdd;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Sets {@link Router#usage} to 0.
	 */
	public void clear() {
		usage = 0;
	}
}
