package ptsp.network;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * A fragment of a {@link Packet}. This unit gets sent around the {@link Network}.
 */
@Data
@AllArgsConstructor
public class Fragment {

	private final Packet packet;

	private final int size;

	private int currentRouterFromId;

	private int currentPath;

	private int pathProgress;

}
