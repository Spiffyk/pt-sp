package ptsp.network;

import lombok.*;
import ptsp.timeline.Frame;

import java.util.*;

/**
 * The main graph class.
 */
@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class Network {

	/**
	 * How many times a packet can fail (get stuck) before it is discarded.
	 */
	private static final int MAX_FAILS = 10;

	/**
	 * A queue of {@link Frame}s. Sorted by time.
	 */
	private final Queue<Frame> timeline = new ArrayDeque<>();

	/**
	 * The packets to be sent the next tick.
	 */
	private final List<Packet> pendingPackets = new ArrayList<>();

	/**
	 * The packets currently being processed. The {@link Fragment}s of those should be placed in the graph.
	 */
	private final List<Packet> activePackets = new ArrayList<>();

	/**
	 * The packets that got stuck the last tick.
	 */
	private final List<Packet> failedPackets = new ArrayList<>();

	/**
	 * The packets that failed more than {@link Network#MAX_FAILS} times.
	 */
	private final List<Packet> lostPackets = new ArrayList<>();

	/**
	 * The packets that successfully reached their destination.
	 */
	private final List<Packet> finishedPackets = new ArrayList<>();

	/**
	 * The fragments currently placed in the graph.
	 */
	private final List<Fragment> fragments = new ArrayList<>();

	/**
	 * The routers in the graph.
	 */
	private final Router[] routers;

	/**
	 * The lines between routers.
	 */
	private final Map<Integer, Line>[] lineMaps;

	/**
	 * A cache of paths between routers.
	 */
	private final Map<Integer, int[][]>[] pathMaps;


	/**
	 * The next timeline {@link Frame} to be processed once its time comes.
	 */
	private Frame pendingFrame = null;

	/**
	 * The number of elapsed ticks.
	 */
	private long time = 0;

	/**
	 * Whether the simulation has finished.
	 */
	private boolean simulationDone = false;

	/**
	 * The amount of packets created.
	 */
	private int sentPackets = 0;


	/**
	 * Creates a new network.
	 *
	 * @param routers The routers in the networks
	 * @param lineMaps The matrix of lines in the network
	 */
	public Network(@NonNull final Router[] routers, @NonNull final Map<Integer, Line>[] lineMaps) {
		this.routers = routers;
		this.lineMaps = lineMaps;

		this.pathMaps = new HashMap[routers.length];
		for (int i = 0; i < pathMaps.length; i++) {
			this.pathMaps[i] = new HashMap<>();
		}
	}



	/**
	 * Processes one tick on the network.
	 */
	public void tick() {
		System.out.format("----- Tick #%d -----\n", time);

		for (final Map<Integer, Line> lineMap : lineMaps) {
			for (final Line line : lineMap.values()) {
				line.reset();
			}
		}

		for (final Router router : routers) {
			if (router != null) {
				router.clear();
			}
		}

		if (pendingFrame == null) {
			pendingFrame = timeline.poll();
		}

		if (pendingFrame != null && time == pendingFrame.getTime()) {
			final List<Packet> packetList = pendingFrame.getPackets();
			sentPackets += packetList.size();
			pendingPackets.addAll(packetList);
			pendingFrame = null;
		}

		{
			final Iterator<Packet> pendingPacketsI = pendingPackets.iterator();
			while (pendingPacketsI.hasNext()) {
				final Packet packet = pendingPacketsI.next();
				send(packet);
				activePackets.add(packet);
				pendingPacketsI.remove();
			}
		}

		{
			final List<Fragment> toCreate = new ArrayList<>();
			final Iterator<Fragment> fragmentsI = fragments.iterator();
			while (fragmentsI.hasNext()) {
				final Fragment fragment = fragmentsI.next();
				final FragmentTickResult result = tickFragment(fragment);

				if (result.isRemoved()) {
					fragmentsI.remove();
				}
				if (result.isLineFailed()) {
					activePackets.remove(fragment.getPacket());
					pendingPackets.remove(fragment.getPacket());
					System.out.printf("Fragment %s failed randomly on a line.\n", fragment);
				}
				if (result.isFailed()) {
					fragment.getPacket().setReceived(0);
					fragment.getPacket().incrementFails();
					activePackets.remove(fragment.getPacket());
					failedPackets.add(fragment.getPacket());
					System.out.printf("Fragment %s failed.\n", fragment);
				}
				if (result.isFinished()) {
					fragment.getPacket().addReceived(fragment.getSize());
					if (fragment.getPacket().getReceived() >= fragment.getPacket().getSize()) {
						activePackets.remove(fragment.getPacket());
						finishedPackets.add(fragment.getPacket());
						System.out.printf("Packet %s finished.\n",
								fragment.getPacket(),
								fragment.getPacket().getReceived(),
								fragment.getPacket().getSize());
					}
				}
				if (result.getToCreate().size() > 0) {
					toCreate.addAll(result.getToCreate());
					System.out.printf("Adding fragments: %s\n", result.getToCreate());
				}
			}
			fragments.addAll(toCreate);
			fragments.removeIf(f -> failedPackets.contains(f.getPacket())); // destroy all fragments of a failed packet

			final Iterator<Packet> failedPacketsIterator = failedPackets.iterator();
			while(failedPacketsIterator.hasNext()) {
				final Packet packet = failedPacketsIterator.next();
				if (packet.getNoOfFails() > MAX_FAILS) {
					System.out.format("Packet %s got lost after %d attempts.\n", packet, MAX_FAILS);
					lostPackets.add(packet);
					failedPacketsIterator.remove();
				}
			}

			pendingPackets.addAll(failedPackets);
			failedPackets.clear();
		}

		simulationDone = pendingPackets.isEmpty() && pendingFrame == null && activePackets.isEmpty() && timeline.isEmpty();
		time++;
	}

	/**
	 * Updates a fragment.
	 *
	 * @param fragment the fragment to update
	 * @return {@code true} if the fragment has reached its target, otherwise {@code false}.
	 */
	private FragmentTickResult tickFragment(@NonNull final Fragment fragment) {
		final FragmentTickResult result = new FragmentTickResult();
		final Integer targetRouterId = fragment.getPacket().getReceiverId();
		Integer startingRouterId = fragment.getCurrentRouterFromId();
		int pathIndex = fragment.getCurrentPath();

		while (true) {
			final int[][] paths = getPaths(startingRouterId, targetRouterId);
			final Line line =
					lineMaps[paths[pathIndex][fragment.getPathProgress()]]
							.get(paths[pathIndex][fragment.getPathProgress() + 1]);

			if (line.getAvailable() < fragment.getSize()) {
				if (line.getUsage() > (line.getCapacity() / 2)) {
					if (Math.random() > line.getRobustness()) {
						result.setRemoved(true);
						result.setLineFailed(true);
					}
				}
				line.addUsage(fragment.getSize());
				startingRouterId = paths[pathIndex][fragment.getPathProgress()];
				fragment.setCurrentRouterFromId(startingRouterId);
				fragment.setPathProgress(0);

				final int[][] newPaths = getPaths(startingRouterId, targetRouterId);
				boolean foundPath = false;
				for (int i = 0; i < newPaths.length; i++) {
					if (getPathAvailable(newPaths[i]) >= fragment.getSize()) {
						foundPath = true;
						fragment.setCurrentPath(i);
						break;
					}
				}

				if (!foundPath) {
					final List<Fragment> newFragments = new ArrayList<>();
					int remaining = fragment.getSize();

					if (routers[startingRouterId].tryAdd(remaining)) {
						for (int i = 0; i < newPaths.length; i++) {
							int available = Math.min(remaining, getPathAvailable(newPaths[i]));
							remaining -= available;
							newFragments.add(new Fragment(fragment.getPacket(), available, startingRouterId, i, 0));
						}

						result.setRemoved(true);
						if (remaining > 0) {
							newFragments.add(
									new Fragment(
											fragment.getPacket(),
											remaining,
											fragment.getCurrentRouterFromId(),
											fragment.getCurrentPath(),
											fragment.getPathProgress()));
						}
						result.getToCreate().addAll(newFragments);
					} else {
						result.setRemoved(true);
						result.setFailed(true);
					}
				}

				break;
			} else if (paths[pathIndex][fragment.getPathProgress() + 1] == targetRouterId) {
				result.setRemoved(true);
				result.setFinished(true);
				return result;
			} else {
				fragment.setPathProgress(fragment.getPathProgress() + 1);
			}
		}

		return result;
	}

	/**
	 * Gets viable paths between the specified routers (by id).
	 *
	 * @param from a router id
	 * @param to a router id
	 * @return an array of paths
	 */
	private int[][] getPaths(final int from, final int to) {
		if (pathMaps[from].get(to) == null) {
			pathMaps[from].put(to, generatePaths(from, to));
		}

		return pathMaps[from].get(to);
	}

	/**
	 * Creates a {@link Fragment} of the specified packet and places it in the graph.
	 *
	 * @param packet The packet
	 */
	private void send(@NonNull final Packet packet) {
		int[][] paths = getPaths(packet.getSenderId(), packet.getReceiverId());
		int idealPath = getIdealPath(paths);

		fragments.add(new Fragment(packet, packet.getSize(), packet.getSenderId(), idealPath, 0));
	}

	/**
	 * Prints info about the specified packet, its paths, ideal path etc.
	 *
	 * @param packet the packet
	 * @param paths the array of paths
	 * @param idealPath the ideal path
	 * @param idealPathAvailable the available capacity of the ideal path
	 */
	private void printPacketInfo(@NonNull final Packet packet,
								 @NonNull final int[][] paths,
								 @NonNull final int idealPath,
								 @NonNull final int idealPathAvailable) {
		System.out.format("Packet: [sender=%d, receiver=%d, size=%d]\n", packet.getSenderId(), packet.getReceiverId(), packet.getSize());
		for (final int[] path : paths) {
			System.out.print(" - path: " + Arrays.toString(path));
			if (path == paths[idealPath]) {
				System.out.println(" <- path with largest capacity");
			} else {
				System.out.println();
			}
		}
		if (idealPathAvailable >= packet.getSize()) {
			System.out.println(" - Will succeed.");
		} else {
			int routerId = -1;
			for (int i = 1; i < paths[idealPath].length; i++) {
				int lineCapacity = lineMaps[paths[idealPath][i-1]].get(paths[idealPath][i]).getAvailable();
				if (lineCapacity < packet.getSize()) {
					routerId = paths[idealPath][i-1];
					break;
				}
			}
			System.out.format(" - Will fragment at router %d.", routerId);
		}
		System.out.println();
	}

	/**
	 * Gets the index of the ideal path from the specified array of paths.
	 *
	 * @param paths the array of paths
	 * @return index of the ideal path
	 */
	private int getIdealPath(@NonNull final int[][] paths) {
		int idealPath = 0;
		int idealPathAvailable = getPathAvailable(paths[idealPath]);
		for (int i = 1; i < paths.length; i++) {
			int[] path = paths[i];
			int pathAvailable = getPathAvailable(path);
			if (pathAvailable > idealPathAvailable) {
				idealPath = i;
				idealPathAvailable = pathAvailable;
			}
		}

		return idealPath;
	}

	/**
	 * Generates paths from one node to another using a BFS algorithm.
	 *
	 * @param from The starting node
	 * @param to The target node
	 * @return An array of paths.
	 */
	private int[][] generatePaths(final Integer from, final Integer to) {
		final boolean[] visited = new boolean[routers.length];
		final Queue<Integer> queue = new ArrayDeque<>();
		final List<int[]>[] paths = new List[routers.length];
		paths[from] = new ArrayList<>();
		paths[from].add(new int[] {from});

		boolean foundTarget = false;
		queue.add(from);

		while (!queue.isEmpty()) {
			final Integer a = queue.poll();
			for (int i = 0; i < routers.length; i++) {
				if (lineMaps[a].get(i) != null) {
					if (!visited[i]) {
						if (i == to) {
							foundTarget = true;
						}
						if (!foundTarget) {
							queue.add(i);
						}
						visited[i] = true;
					}

					if (paths[i] == null) {
						paths[i] = new ArrayList<>();
					}

					final List<int[]> newPaths = new ArrayList<>();
					for (final int[] origPath : paths[a]) {
						final int[] newPath = Arrays.copyOf(origPath, origPath.length + 1);
						newPath[origPath.length] = i;
						newPaths.add(newPath);
					}
					paths[i].addAll(newPaths);
				}
			}
		}

		return paths[to].toArray(new int[paths[to].size()][]);
	}

	/**
	 * Gets the specified path's available capacity.
	 *
	 * @param path The path for which the available capacity is to be determined
	 * @return The path's available capacity
	 */
	private int getPathAvailable(final int[] path) {
		int resultCapacity = Integer.MAX_VALUE;
		for (int i = 1; i < path.length; i++) {
			int lineCapacity = lineMaps[path[i-1]].get(path[i]).getAvailable();
			if (lineCapacity < resultCapacity) {
				resultCapacity = lineCapacity;
			}
		}
		return resultCapacity;
	}

	/**
	 * The result of a single tick.
	 */
	@Data
	private class FragmentTickResult {

		/**
		 * Whether the fragment should be removed at the end of the tick.
		 */
		private boolean removed = false;

		/**
		 * Whether the fragment finishes the packet.
		 */
		private boolean finished = false;

		/**
		 * Whether the fragment got lost on the way.
		 */
		private boolean failed = false;

		/**
		 * Whether the fragment got lost due to a line failing.
		 */
		private boolean lineFailed = false;

		/**
		 * New fragments to toCreate at the end of the tick.
		 */
		private final List<Fragment> toCreate = new ArrayList<>();

	}
}
